package it.cuebiq.challenge;

import it.cuebiq.challenge.bean.input.Data;
import it.cuebiq.challenge.bean.input.Row;
import it.cuebiq.challenge.bean.output.Node;

import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The class {@code Report} contains methods for create basic
 * operations such custom function with a set of rows.
 */

class Report {

    private final static Logger logger = Logger.getLogger(Report.class.getName());

    /**
     * Don't let anyone instantiate this class.
     */
    private Report() {
    }

    /**
     * Returns a {@code Map<Node, Double>} value.
     *
     * <p>The output is an aggregation tree, which can be queried to obtain the value of the aggregations
     * at any level, for any labeling group.</p>
     *
     * @param data             {@code Data}  @NotNull The data is a collection rows, composed by a numerical value and a label tuple, row i =(L 1i ,L 2i ,L 3i ,..,L ni ,Vi) , so that L ji belongs to Domain j
     * @param aggFunction      {@code Function<Stream<Double>, Double>} @NotNull  The aggregation function can be any function that takes a collection of numerical values as an input and returns a single value
     * @param aggregationOrder {@code List<String>} @NotNull  The aggregation order is the description of the aggregation hierarchy
     * @return The output is an aggregation tree
     */

    public static Map<Node, Double> pivot(Data data, Function<Stream<Double>, Double> aggFunction, List<String> aggregationOrder) throws Exception {
        Objects.requireNonNull(data, "Data must be not null");
        Objects.requireNonNull(data.getRows(), "Rows into data must be not null");
        Objects.requireNonNull(data.getLabels(), "Labels into data must be not null");
        Objects.requireNonNull(aggFunction, "Aggregation Function must be not null");
        Objects.requireNonNull(aggregationOrder, "Aggregation Order must be not null");

        Map<Node, Double> finalTree = new TreeMap();

        if (!data.getLabels().containsAll(aggregationOrder))
            throw new Exception("Aggregation Order List is not correct");

        List<Integer> listIndex = aggregationOrder.stream().map((String ele) -> data.getLabels().indexOf(ele)).collect(Collectors.toList());
        listIndex.forEach(entry ->
                data
                        .getRows()
                        .stream()
                        .collect(Collectors.groupingBy((Row x) -> {
                            List<Integer> listKeyToAdd = listIndex.subList(0, listIndex.indexOf(entry) + 1);
                            List<String> labels = new ArrayList();
                            listKeyToAdd.forEach(key ->
                                    labels.add(x.getLabel().get(key))
                            );
                            return labels;
                        }))
                        .forEach((key, value) -> {
                            Stream<Double> val = value.stream().map(Row::getValue);
                            finalTree.put(new Node(String.join(" - ", key), key.size()), aggFunction.apply(val));
                        })
        );
        addTotal(data.getRows(), aggFunction, finalTree);
        return finalTree;
    }

    /**
     * The method apply the custom function at all row and create a new node with the total function
     *
     * @param rows        {@code List<Row>} list of rows to whom the function will be applied
     * @param aggFunction {@code Function<Stream<Double>, Double>} custom function
     * @param finalTree   {@code  Map<Node, Double>} tree final that include total function
     */
    private static void addTotal(List<Row> rows, Function<Stream<Double>, Double> aggFunction, Map<Node, Double> finalTree) {

        if (rows.size() != 0) {
            Stream<Double> listValue = rows.stream().map((element -> element.getValue()));

            Double total = aggFunction.apply(listValue);

            finalTree.put(new Node("Total", 0), total);
        }
    }
}

