package it.cuebiq.challenge.bean.input;

import java.util.List;
import java.util.Objects;

/**
 * Bean that defines a single row of the input data for the execution of the Pivot algorithm
 */
public class Row {

    private List<String> label;

    private Double value;

    /**
     * Constructor with two mandatory parameters
     *
     * @param label {@code List<String>} the list of hierarchy names
     * @param value {@code Double} value associate
     */
    public Row(List<String> label, Double value) {
        this.label = label;
        this.value = value;
    }

    /**
     * Return a the list of hierarchy names
     *
     * @return {@code List<String>}
     */
    public List<String> getLabel() {
        return label;
    }

    /**
     * Set a list of hierarchy names
     *
     * @param label {@code List<String>} list of hierarchy names
     */
    public void setLabel(List<String> label) {
        this.label = label;
    }

    /**
     * Return a single hierarchy value
     *
     * @return {@code Double}
     */
    public Double getValue() {
        return value;
    }

    /**
     * Set a hierarchy value
     *
     * @param value {@code Double} hierarchy value
     */
    public void setValue(Double value) {
        this.value = value;
    }

    /**
     * Compares the specified object with this list for equality.
     * Returns true if and only if the specified object is all corresponding pairs of elements in the two lists are equal.
     * In other words, two ojject are defined to be equal if they contain the same elements in the same order.
     * For this object first the labal and the the value
     *
     * @param o – the object to be compared for equality with this list
     * @return true if the specified object is equal to this list
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Row row = (Row) o;
        return label.equals(row.label) &&
                value.equals(row.value);
    }

    /**
     * Generates a hash code for a sequence of input values(label, value)
     *
     * @return a hash value of the sequence of input values
     */
    @Override
    public int hashCode() {
        return Objects.hash(label, value);
    }
}
