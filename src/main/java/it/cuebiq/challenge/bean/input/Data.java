
package it.cuebiq.challenge.bean.input;

import java.util.List;

/**
 * Bean that defines the input data for the execution of the Pivot algorithm
 */
public class Data {

    private List<String> labels;

    private List<Row> rows;

    /**
     * Constructor with two mandatory parameters
     *
     * @param labels list of label that describe the content of rows
     * @param rows   list of row
     */
    public Data(List<String> labels, List<Row> rows) {
        this.labels = labels;
        this.rows = rows;
    }

    /**
     * Return a list of labels
     *
     * @return {@code List<String>}
     */
    public List<String> getLabels() {
        return labels;
    }

    /**
     * Set a list of labels
     *
     * @param labels {@codeList<String>} list of labels
     */
    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    /**
     * Return a list of element rows
     *
     * @return {@code List<Row>}
     */
    public List<Row> getRows() {
        return rows;
    }

    /**
     * Set a list of rows
     *
     * @param rows {@codeList<Element>} list of rows
     */
    public void setRows(List<Row> rows) {
        this.rows = rows;
    }
}
