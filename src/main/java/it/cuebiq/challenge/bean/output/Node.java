package it.cuebiq.challenge.bean.output;

import java.util.Comparator;
import java.util.Objects;

/**
 * Test Node for Data
 */

/**
 * Bean that defines a Node of Tree for the output  of  the Pivot algorithm execution
 */
public class Node implements Comparable<Node> {

    private Integer level;
    private String key;

    /**
     * Constructor with two mandatory parameters
     *
     * @param key   the key of the node in the  TreeSet
     * @param level level of hierarchy label
     */
    public Node(String key, int level) {
        this.level = level;
        this.key = key;
    }

    /**
     * Return a level of hierarchy
     *
     * @return {@code Integer}
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * Set a level of hierarchy
     *
     * @param level {@code String} a level of hierarchy
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * Return a key of node into Tree
     *
     * @return {@code String}
     */
    public String getKey() {
        return key;
    }

    /**
     * Set a key of node into Tree
     *
     * @param key {@code String}  key of node
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Compares the specified object with this list for equality.
     * Returns true if and only if the specified object is all corresponding pairs of elements in the two lists are equal.
     * In other words, two ojject are defined to be equal if they contain the same elements in the same order.
     * For this object first the key and the level
     *
     * @param o – the object to be compared for equality with this list
     * @return true if the specified object is equal to this list
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return key.equals(node.key) && Objects.equals(level, node.level);
    }

    /**
     * Generates a hash code for a sequence of input values(level, key)
     *
     * @return a hash value of the sequence of input values
     */
    @Override
    public int hashCode() {
        return Objects.hash(level, key);
    }

    @Override
    public int compareTo(Node o) {
        return Comparator.comparing(Node::getKey)
                .thenComparing(Node::getLevel)
                .compare(this, o);

    }

    /**
     * Override toString Metod for write only key of Node
     *
     * @return key of Node
     */
    @Override
    public String toString() {
        return key;
    }
}