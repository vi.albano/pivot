package it.cuebiq.challenge;

import it.cuebiq.challenge.bean.input.Data;
import it.cuebiq.challenge.bean.input.Row;
import it.cuebiq.challenge.bean.output.Node;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * Unit test for simple Report.
 */
public class ReportTest {

    private final static Logger logger = Logger.getLogger(ReportTest.class.getName());

    private final List<String> nation = Arrays.asList("Germany", "Spain", "France", "Italy");
    private final List<String> eyes = Arrays.asList("Green", "Blue", "Dark", "Brown", "Green");
    private final List<String> hair = Arrays.asList("Brown", "Black", "Red", "Blonde", "Violet");

    @Test
    public void when_MassiveRandomDataForMilionRecod_Expect_LessOneSecond() {

        Random rand = new Random();


        List<Row> rows = new ArrayList();

        IntStream.range(0, 1000000).forEach(entry -> {
            String randonNation = nation.get(rand.nextInt(nation.size()));
            String randonEyes = eyes.get(rand.nextInt(eyes.size()));
            String randonHair = hair.get(rand.nextInt(hair.size()));
            rows.add(new Row(Arrays.asList(randonNation, randonEyes, randonHair), rand.nextDouble()));
        });


        Data data = new Data(Arrays.asList("Nation", "Eyes", "Hair"), rows);
        assertEquals(rows.size(), 1000000);
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).sum());
        Instant start = Instant.now();
        try {
            Report.pivot(data, sum, Arrays.asList("Nation", "Eyes", "Hair"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instant end = Instant.now();
        assertTrue(Duration.between(start, end).toNanos() <= Duration.ofSeconds(10).toNanos());
        logger.info("Seconds: " + Duration.between(start, end).getSeconds() + " for 1000000 record");
    }

    @Test
    public void when_NationIsGerman_Expect_561People() throws Exception {

        List<Row> rows = new ArrayList();

        rows.add(new Row(Arrays.asList("Germany", "Green", "Brown"), Double.valueOf(100)));
        rows.add(new Row(Arrays.asList("Germany", "Green", "Black"), Double.valueOf(110)));
        rows.add(new Row(Arrays.asList("Germany", "Green", "Red"), Double.valueOf(120)));
        rows.add(new Row(Arrays.asList("Germany", "Green", "Blonde"), Double.valueOf(130)));
        rows.add(new Row(Arrays.asList("Germany", "Dark", "Violet"), Double.valueOf(101)));

        Data data = new Data(Arrays.asList("Nation", "Eyes", "Hair"), rows);

        assertEquals(rows.size(), 5);
        //Defined a sum function
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).sum());

        List<String> hierarchy = Arrays.asList("Nation", "Eyes", "Hair");

        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);

        assertEquals("Number of row in the report", report.size(), 9);
        assertEquals("Result of value report for Germany", report.get(new Node("Germany", 1)), Double.valueOf(561));
        assertEquals("Result of value report for Germany - Green", report.get(new Node("Germany - Green", 2)), Double.valueOf(460));

        assertEquals("Result of value report for Germany - Green - Brown", report.get(new Node("Germany - Green - Brown", 3)), Double.valueOf(100));
        assertEquals("Result of value report for Germany - Green - Black", report.get(new Node("Germany - Green - Black", 3)), Double.valueOf(110));
        assertEquals("Result of value report for Germany - Green - Red", report.get(new Node("Germany - Green - Red", 3)), Double.valueOf(120));
        assertEquals("Result of value report for Germany - Green - Blonde", report.get(new Node("Germany - Green - Blonde", 3)), Double.valueOf(130));

        assertEquals("Result of value report for Germany - Dark", report.get(new Node("Germany - Dark", 2)), Double.valueOf(101));

        assertEquals("Result of value report for Germany - Dark - Violet", report.get(new Node("Germany - Dark - Violet", 3)), Double.valueOf(101));
        assertEquals("Result  of value report for all Nation", report.get(new Node("Total", 0)), Double.valueOf(561));

    }


    @Test
    public void when_NationIsItalian_Expect_60People() throws Exception {

        List<Row> rows = new ArrayList();

        rows.add(new Row(Arrays.asList("Italy", "Green", "Brown"), Double.valueOf(10)));
        rows.add(new Row(Arrays.asList("Italy", "Black", "Black"), Double.valueOf(11)));
        rows.add(new Row(Arrays.asList("Italy", "Violet", "Red"), Double.valueOf(12)));
        rows.add(new Row(Arrays.asList("Italy", "Green", "Blonde"), Double.valueOf(13)));
        rows.add(new Row(Arrays.asList("Italy", "Dark", "Violet"), Double.valueOf(14)));

        Data data = new Data(Arrays.asList("Nation", "Eyes", "Hair"), rows);

        assertEquals(rows.size(), 5);
        //Defined a sum function
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).sum());

        List<String> hierarchy = Arrays.asList("Nation", "Eyes");

        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);

        assertEquals("Number of row in the report", report.size(), 6);
        assertEquals("Result of value report for Italy", report.get(new Node("Italy", 1)), Double.valueOf(60));
        assertEquals("Result of value report for Italy - Green", report.get(new Node("Italy - Green", 2)), Double.valueOf(23));
        assertEquals("Result of value report for Italy - Black", report.get(new Node("Italy - Black", 2)), Double.valueOf(11));
        assertEquals("Result of value report for Italy - Violet", report.get(new Node("Italy - Violet", 2)), Double.valueOf(12));
        assertEquals("Result of value report for Italy - Dark", report.get(new Node("Italy - Dark", 2)), Double.valueOf(14));
        assertEquals("Result  of value report for all Nation", report.get(new Node("Total", 0)), Double.valueOf(60));


    }

    @Test
    public void when_NationIsFrance_Expect_60People() throws Exception {

        List<Row> rows = new ArrayList();

        rows.add(new Row(Arrays.asList("France", "Green", "Brown"), Double.valueOf(10)));
        rows.add(new Row(Arrays.asList("France", "Black", "Black"), Double.valueOf(11)));
        rows.add(new Row(Arrays.asList("France", "Violet", "Red"), Double.valueOf(12)));
        rows.add(new Row(Arrays.asList("France", "Green", "Blonde"), Double.valueOf(13)));
        rows.add(new Row(Arrays.asList("France", "Dark", "Violet"), Double.valueOf(14)));
        rows.add(new Row(Arrays.asList("France", "Blonde", "Red"), Double.valueOf(14)));

        Data data = new Data(Arrays.asList("Nation", "Eyes", "Hair"), rows);

        assertEquals(rows.size(), 6);
        //Defined a sum function
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).sum());

        List<String> hierarchy = Arrays.asList("Nation", "Hair");

        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);

        assertEquals("Number of row in the report", report.size(), 7);
        assertEquals("Result of value report for France", report.get(new Node("France", 1)), Double.valueOf(74));
        assertEquals("Result of value report for France - Brown", report.get(new Node("France - Brown", 2)), Double.valueOf(10));
        assertEquals("Result of value report for France - Black", report.get(new Node("France - Black", 2)), Double.valueOf(11));
        assertEquals("Result of value report for France - Red", report.get(new Node("France - Red", 2)), Double.valueOf(26));
        assertEquals("Result of value report for France - Blonde", report.get(new Node("France - Blonde", 2)), Double.valueOf(13));
        assertEquals("Result of value report for France - Violet", report.get(new Node("France - Violet", 2)), Double.valueOf(14));
        assertEquals("Result  of value report for all Nation", report.get(new Node("Total", 0)), Double.valueOf(74));

    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();


    @Test
    public void when_AggregationListInNotCorrect_Expect_Exception() throws Exception {

        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("Aggregation Order List is not correct");

        List<Row> rows = new ArrayList();

        rows.add(new Row(Arrays.asList("France", "Green", "Brown"), Double.valueOf(10)));
        rows.add(new Row(Arrays.asList("France", "Black", "Black"), Double.valueOf(11)));
        rows.add(new Row(Arrays.asList("France", "Violet", "Red"), Double.valueOf(12)));
        rows.add(new Row(Arrays.asList("France", "Green", "Blonde"), Double.valueOf(13)));
        rows.add(new Row(Arrays.asList("France", "Dark", "Violet"), Double.valueOf(14)));
        rows.add(new Row(Arrays.asList("France", "Blonde", "Red"), Double.valueOf(14)));

        Data data = new Data(Arrays.asList("Nation", "Eyes", "Hair"), rows);

        assertEquals(rows.size(), 6);
        //Defined a sum function
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).sum());

        List<String> hierarchy = Arrays.asList("Nation", "Hair", "TOT");

        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);


    }


    @Test
    public void when_NationIsGermanItalyAndSpain_Expect_561People() throws Exception {

        List<Row> rows = new ArrayList();

        rows.add(new Row(Arrays.asList("Germany", "Blue", "Red"), Double.valueOf(120)));
        rows.add(new Row(Arrays.asList("Germany", "Green", "Black"), Double.valueOf(110)));
        rows.add(new Row(Arrays.asList("Germany", "Green", "Brown"), Double.valueOf(100)));
        rows.add(new Row(Arrays.asList("Germany", "Violet", "Blonde"), Double.valueOf(130)));
        rows.add(new Row(Arrays.asList("Germany", "Dark", "Violet"), Double.valueOf(101)));

        rows.add(new Row(Arrays.asList("Italy", "Green", "Brown"), Double.valueOf(100)));
        rows.add(new Row(Arrays.asList("Italy", "Brown", "Black"), Double.valueOf(110)));
        rows.add(new Row(Arrays.asList("Italy", "Blue", "Red"), Double.valueOf(120)));
        rows.add(new Row(Arrays.asList("Italy", "Green", "Blonde"), Double.valueOf(130)));
        rows.add(new Row(Arrays.asList("Italy", "Dark", "Violet"), Double.valueOf(101)));

        rows.add(new Row(Arrays.asList("Spain", "Green", "Brown"), Double.valueOf(100)));
        rows.add(new Row(Arrays.asList("Spain", "Brown", "Black"), Double.valueOf(110)));
        rows.add(new Row(Arrays.asList("Spain", "Green", "Red"), Double.valueOf(120)));
        rows.add(new Row(Arrays.asList("Spain", "Blue", "Blonde"), Double.valueOf(130)));
        rows.add(new Row(Arrays.asList("Spain", "Dark", "Violet"), Double.valueOf(101)));

        Data data = new Data(Arrays.asList("Nation", "Eyes", "Hair"), rows);

        assertEquals(rows.size(), 15);
        //Defined a sum function
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).average().orElse(0));

        List<String> hierarchy = Arrays.asList("Nation", "Eyes", "Hair");

        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);

        assertEquals("Number of row in the report", report.size(), 31);
        assertEquals("Result of value report for Germany", report.get(new Node("Germany", 1)), Double.valueOf(112.2));
        assertEquals("Result of value report for Germany - Green", report.get(new Node("Germany - Green", 2)), Double.valueOf(105));

        assertEquals("Result of value report for Germany - Green - Brown", report.get(new Node("Germany - Green - Brown", 3)), Double.valueOf(100));
        assertEquals("Result of value report for Germany - Green - Black", report.get(new Node("Germany - Green - Black", 3)), Double.valueOf(110));


        assertEquals("Result of value report for Germany - Dark", report.get(new Node("Germany - Dark", 2)), Double.valueOf(101));

        assertEquals("Result of value report for Germany - Dark - Violet", report.get(new Node("Germany - Dark - Violet", 3)), Double.valueOf(101));
        assertEquals("Result  of value report for all Nation", report.get(new Node("Total", 0)), Double.valueOf(112.2));

    }


    @Test
    public void when_AggregationListIsEmptyAndDataRowIsEmpty_Expect_ReportWithZeroNode() throws Exception {

        List<Row> rows = new ArrayList();
        Data data = new Data(Arrays.asList(), rows);
        assertEquals(rows.size(), 0);
        //Defined a sum function
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).sum());
        List<String> hierarchy = Arrays.asList();
        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);
        assertEquals("Number of row in the report", report.size(), 0);
    }

    @Test
    public void when_AggregationListIsEmptyCustomFunctionIsDefinedAndDataIsNull_Expect_NullPointException() throws Exception {

        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Data must be not null");
        Data data = null;
        assertEquals(data, null);
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).sum());
        List<String> hierarchy = Arrays.asList();
        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);
    }

    @Test
    public void when_AggregationListIsEmptyCustomFunctionIsEmptyAndRowsIntoDataIsNull_Expect_NullPointException() throws Exception {

        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Rows into data must be not null");
        List<Row> rows = null;
        Data data = new Data(Arrays.asList(), rows);
        assertNull(data.getRows());
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).sum());
        List<String> hierarchy = Arrays.asList();
        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);

    }

    @Test
    public void when_AggregationListIsEmptyCustomFunctionIsEmptyAndLabelsIntoDataIsNull_Expect_NullPointException() throws Exception {

        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Labels into data must be not null");
        List<Row> rows = new ArrayList();
        List<String> labels = null;
        Data data = new Data(labels, rows);
        assertEquals(rows.size(), 0);
        assertNull(data.getLabels());
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).sum());
        List<String> hierarchy = Arrays.asList();
        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);
    }

    @Test
    public void when_AggregationListIsNullCustomFunctionIsEmptyAndLabelsIntoDataIsEmpty_Expect_NullPointException() throws Exception {

        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Aggregation Order must be not null");
        List<Row> rows = new ArrayList();
        List<String> labels = Arrays.asList();
        Data data = new Data(labels, rows);
        assertEquals(rows.size(), 0);
        Function<Stream<Double>, Double> sum = (Stream<Double> x1) -> (x1.mapToDouble(i -> i).sum());
        List<String> hierarchy = null;
        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);

    }

    @Test
    public void when_AggregationListIsNullCustomFunctionIsNullAndLabelsIntoDataIsEmpty_Expect_NullPointException() throws Exception {

        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Aggregation Function must be not null");
        List<Row> rows = new ArrayList();
        List<String> labels = Arrays.asList();
        Data data = new Data(labels, rows);
        assertEquals(rows.size(), 0);
        Function<Stream<Double>, Double> sum = null;
        List<String> hierarchy = null;
        Map<Node, Double> report = Report.pivot(data, sum, hierarchy);

    }


}
