package it.cuebiq.challenge.bean;

import it.cuebiq.challenge.bean.input.Row;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class RowTest {

    @Test
    public void when_getLabelIsTheSameOfConstructorParameter_Expect_True() {
        Row row = new Row(Arrays.asList("Italy", "France"), 15d);

        assertEquals(row.getLabel(), Arrays.asList("Italy", "France"));
    }

    @Test
    public void when_setLabelWithDifferentValueFromConstructor_Expect_True() {
        Row row = new Row(Arrays.asList("Italy", "France"), 15d);
        row.setLabel(Arrays.asList("Italy", "Germany"));
        assertEquals(row.getLabel(), Arrays.asList("Italy", "Germany"));
    }

    @Test
    public void when_getValueIsTheSameOfConstructorParameter_Expect_True() {
        Row row = new Row(Arrays.asList("Italy", "France"), 15d);

        assertEquals(row.getValue().intValue(), 15);
    }

    @Test
    public void when_setValueWithDifferentValueFromConstructor_Expect_True() {

        Row row = new Row(Arrays.asList("Italy", "France"), 15d);
        row.setValue(10000d);
        assertEquals(row.getValue().intValue(), 10000);
    }

    @Test
    public void when_equalsWithTheSameObjcet_Expect_True() {
        Row row1 = new Row(Arrays.asList("Italy", "France"), 15d);
        Row row2 = new Row(Arrays.asList("Italy", "France"), 15d);
        assertEquals(row1, row2);


    }

    @Test
    public void when_equalsWithTheDefferentObjcet_Expect_False() {
        Row row1 = new Row(Arrays.asList("Italy", "France"), 15d);
        Row row2 = new Row(Arrays.asList("Italy", "France"), 20d);
        assertNotEquals(row1, row2);

    }

    @Test
    public void when_hasCodeWithTheSameObjcet_Expect_True() {
        Row row1 = new Row(Arrays.asList("Italy", "France"), 15d);
        Row row2 = new Row(Arrays.asList("Italy", "France"), 15d);
        assertEquals(row1.hashCode(), row2.hashCode());


    }

    @Test
    public void when_hasCodeWithTheDefferentObjcet_Expect_False() {
        Row row1 = new Row(Arrays.asList("Italy", "France"), 15d);
        Row row2 = new Row(Arrays.asList("Italy", "France"), 26d);

        assertNotEquals(row1.hashCode(), row2.hashCode());
    }
}