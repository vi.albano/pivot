package it.cuebiq.challenge.bean;

import it.cuebiq.challenge.bean.input.Data;
import it.cuebiq.challenge.bean.input.Row;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test Unit for Data
 */
public class DataTest {


    @Test
    public void when_getLabelsIsTheSameOfConstructorParameter_Expect_True() {
        List<Row> rows = new ArrayList();
        rows.add(new Row(Arrays.asList("Germany"), 100d));
        Data data = new Data(Arrays.asList("Nation"), rows);

        assertEquals(data.getLabels(), Arrays.asList("Nation"));
    }

    @Test
    public void when_setLabelsWithDifferentValueFromConstructor_Expect_True() {
        List<Row> rows = new ArrayList();
        rows.add(new Row(Arrays.asList("Milan"), 100d));
        Data data = new Data(Arrays.asList("City"), rows);
        data.setLabels(Arrays.asList("City"));
        assertEquals(data.getLabels(), Arrays.asList("City"));
    }

    @Test
    public void when_getRowsIsTheSameOfConstructorParameter_Expect_True() {
        List<Row> rows = new ArrayList();
        rows.add(new Row(Arrays.asList("Germany"), 100d));
        Data data = new Data(Arrays.asList("Nation"), rows);
        assertEquals(data.getRows(), rows);
        assertEquals(data.getRows().size(), 1);
    }

    @Test
    public void when_setRowseWithDifferentValueFromConstructor_Expect_True() {

        List<Row> rows = new ArrayList();
        rows.add(new Row(Arrays.asList("Germany"), 100d));
        Data data = new Data(Arrays.asList("Nation"), rows);
        assertEquals(data.getRows(), rows);
        assertEquals(data.getRows().size(), 1);

        List<Row> newRows = new ArrayList();
        newRows.add(new Row(Arrays.asList("Germany"), 100d));
        newRows.add(new Row(Arrays.asList("Italy"), 10d));
        data.setRows(newRows);

        assertEquals(data.getRows(), newRows);
        assertEquals(data.getRows().size(), 2);
    }
}