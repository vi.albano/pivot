package it.cuebiq.challenge.bean;

import it.cuebiq.challenge.bean.output.Node;
import org.junit.Test;

import static org.junit.Assert.*;

public class NodeTest {


    @Test
    public void when_getKeyIsTheSameOfConstructorParameter_Expect_True() {
        Node node = new Node("Italy", 1);

        assertEquals(node.getKey(), "Italy");
    }

    @Test
    public void when_setKeyWithDifferentValueFromConstructor_Expect_True() {
        Node node = new Node("Italy", 1);
        node.setKey("France");
        assertEquals(node.getKey(), "France");
    }

    @Test
    public void when_getLevelIsTheSameOfConstructorParameter_Expect_True() {
        Node node = new Node("Italy", 1);
        assertEquals(node.getLevel().intValue(), 1);
    }

    @Test
    public void when_setLevelWithDifferentValueFromConstructor_Expect_True() {
        Node node = new Node("Italy", 1);
        node.setLevel(10);
        assertEquals(node.getLevel().intValue(), 10);
    }

    @Test
    public void when_equalsWithTheSameObjcet_Expect_True() {
        Node node1 = new Node("Italy", 1);
        Node node2 = new Node("Italy", 1);
        assertTrue(node1.equals(node2));


    }

    @Test
    public void when_equalsWithTheDefferentObjcet_Expect_False() {
        Node node1 = new Node("Italy", 1);
        Node node2 = new Node("Italy", 5);
        assertFalse(node1.equals(node2));

    }

    @Test
    public void when_hasCodeWithTheSameObjcet_Expect_True() {
        Node node1 = new Node("Italy", 1);
        Node node2 = new Node("Italy", 1);
        assertTrue(node1.hashCode() == node2.hashCode());

    }

    @Test
    public void when_hasCodeWithTheDefferentObjcet_Expect_False() {
        Node node1 = new Node("Italy", 1);
        Node node2 = new Node("Italy", 2);

        assertFalse(node1.hashCode() == node2.hashCode());
    }

    @Test
    public void when_toString_Expect_ConcatWithKeyAndLevel() {
        Node node1 = new Node("Italy", 1);

        assertTrue(node1.toString().equals("Italy"));
    }


    @Test
    public void when_compareToWithSameObjects_ExpectTre() {

        Node node1 = new Node("Italy", 1);
        Node node2 = new Node("Italy", 1);

        assertTrue(node1.compareTo(node2) == 0);
    }

    @Test
    public void when_compareToWithDIfferenteObjects_ExpectTre() {

        Node node1 = new Node("Italy", 1);
        Node node2 = new Node("Italy", 2);

        assertTrue(node1.compareTo(node2) <= -1);

        Node node7 = new Node("Italy", 1);
        Node node8 = new Node("Italy", 88);

        assertTrue(node7.compareTo(node8) <= -1);

        Node node3 = new Node("Zurich", 1);
        Node node4 = new Node("Italy", 1);

        assertTrue(node3.compareTo(node4) >= 1);

        Node node5 = new Node("UK", 1);
        Node node6 = new Node("Italy", 1);

        assertTrue(node5.compareTo(node6) >= 1);
    }

}